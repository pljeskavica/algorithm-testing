import msToTime from './msToTime';
import now from 'performance-now'

function start(tests, args, repeatCount = 10000){
  for(let i= 0; i < tests.length; i ++){
    let id = i+1;
    let times = [];
    for(let j = 0; j < repeatCount; j++){
      const startTime = now();
      const answer = tests[i](...args);
      const endTime = now();
      times.push(endTime - startTime);

      if(j==0){
        // answers.push[answer]
        console.log("The answer is: "+ answer)
      }
    }
  let count = times.length;
  times = times.reduce((previous, current) => current += previous);
  // console.log(times)
  // console.log(count)
  times /= count;
  // console.log(times)

  console.log(`Approach ${id} took ${msToTime(times)} on average to complete.`)
  }
  
  // console.log(`Approach ${id}:`)
  // console.log(times)
 
}

export default start;