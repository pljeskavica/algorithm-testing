function msToTime(duration) {
      if(duration < 1){
        const microseconds = duration * 1000;
        return `${microseconds.toFixed(3)} microseconds`;
        if(microseconds < 1){
          const nanoseconds = microseconds * 1000;
          return `${nanoseconds.toFixed(3)} nanoseconds`;
        }
      }
      if(duration < 1000) {
        return `${duration.toFixed(3)} milliseconds`;
      }
      var milliseconds = parseInt((duration%1000)/100)
          , seconds = parseInt((duration/1000)%60)
          , minutes = parseInt((duration/(1000*60))%60)
          , hours = parseInt((duration/(1000*60*60))%24);

      hours = (hours < 10) ? "0" + hours : hours;
      minutes = (minutes < 10) ? "0" + minutes : minutes;
      seconds = (seconds < 10) ? "0" + seconds : seconds;

      return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
};

export default msToTime;