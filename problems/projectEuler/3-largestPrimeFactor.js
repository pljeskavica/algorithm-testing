import {primes} from '../data/primes';

export const test1 = (n) => {
  let highest = 0,
      currentN = n;
  for (let i = 0; i < primes.length; i++){
    // console.log(primes.indexOf(currentN))
    if(primes.indexOf(currentN) != -1){
      return (currentN > highest ? currentN : highest);
    }
    if(currentN % primes[i] === 0){
      highest = primes[i];
      currentN /= primes[i];
      i -= 1;
    }
  }
}

export const test2 = (n, primeIndex = 0, highest = 0) => {
  if(primes.indexOf(n) != -1){
    return (n > highest ? n : highest);
  }
  if(n % primes[primeIndex] === 0){
    return(test2(n/primes[primeIndex], primeIndex, primes[primeIndex]))
  }
  return(test2(n, primeIndex+1, highest));
}