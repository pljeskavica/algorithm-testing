// Find the difference of the sum of the squares of the first n natural numbers and the square of the sum of the first n natural numbers

// O(n) Time
// O(1) Memory
export const test1 = (n) => {
  // Create holder for the Sum of the numbers
  let sumOfNumbers = 0,
  // Create holder for the sum of the squares
      sumOfSquares = 0;

  // Iterate through the numbers from 1 - n
  for(let i = 1; i<=n; i++){
     // Add i to the Sum holder
     sumOfNumbers += i;
    // Square i and add it to the sum of squares holder
    sumOfSquares += Math.pow(i, 2);
  }
  // Square the holder of the sum  of the numbers
  // Return SquareOfSum - SumOfSquares
  return (Math.pow(sumOfNumbers, 2) - sumOfSquares);
}

// O(n) Time
// O(n) Memory - TCO is not supported in node currently
export const test2 = (n, i=1, sumOfNumbers=0, sumOfSquares=0) => {
  if(i>n){
    return (Math.pow(sumOfNumbers, 2) - sumOfSquares);
  }
  return test2(n, i+1, sumOfNumbers+i, sumOfSquares+Math.pow(i, 2));
}