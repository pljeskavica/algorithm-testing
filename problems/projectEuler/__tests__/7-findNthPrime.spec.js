import {test1, test2, test3, test4, test5} from '../7-findNthPrime';

describe('Should find Nth prime number using approach 1', () => {
  it('Should find the sixth Prime Number', () => {
    expect(test1(6)).toBe(13)
  });
  it('Should find the 15th Prime Number', () => {
    expect(test1(15)).toBe(47)
  });
  it('Should find the 10001 Prime Number', () => {
    expect(test1(10001)).toBe(104743)
  });
  // it('Should find the 150050th Prime Number', () => {
  //   expect(test1(150050)).toBe(2015869)
  // });
});
describe('Should find Nth prime number using approach 2', () => {
  it('Should find the sixth Prime Number', () => {
    expect(test2(6)).toBe(13)
  });
  it('Should find the 15th Prime Number', () => {
    expect(test2(15)).toBe(47)
  });
  it('Should find the 10001 Prime Number', () => {
    expect(test2(10001)).toBe(104743)
  });
  // it('Should find the 150050th Prime Number', () => {
  //   expect(test1(150050)).toBe(2015869)
  // });
});

describe('Should find Nth prime number using approach 3', () => {
  it('Should find the sixth Prime Number', () => {
    expect(test3(6)).toBe(13)
  });
  it('Should find the 15th Prime Number', () => {
    expect(test3(15)).toBe(47)
  });
  it('Should find the 10001 Prime Number', () => {
    expect(test3(10001)).toBe(104743)
  });
  // it('Should find the 150050th Prime Number', () => {
  //   expect(test1(150050)).toBe(2015869)
  // });
});

describe('Should find Nth prime number using approach 4', () => {
  it('Should find the sixth Prime Number', () => {
    expect(test4(6)).toBe(13)
  });
  it('Should find the 15th Prime Number', () => {
    expect(test4(15)).toBe(47)
  });
  it('Should find the 10001 Prime Number', () => {
    expect(test4(10001)).toBe(104743)
  });
  // it('Should find the 150050th Prime Number', () => {
  //   expect(test4(150050)).toBe(2015869)
  // });
});

describe('Should find Nth prime number using approach 5', () => {
  it('Should find the sixth Prime Number', () => {
    expect(test5(6)).toBe(13)
  });
  it('Should find the 15th Prime Number', () => {
    expect(test5(15)).toBe(47)
  });
  it('Should find the 10001 Prime Number', () => {
    expect(test5(10001)).toBe(104743)
  });
  // it('Should find the 150050th Prime Number', () => {
  //   expect(test5(150050)).toBe(2015869)
  // });
});