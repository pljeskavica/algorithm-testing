import * as tests from '../4-largestPalindrome';

describe('To Find the largest palindrome made from the product of integers using Approach 1', () => {
  it('Should find largest from 2 digit integers', () => {
    expect(tests.test1(2)).toBe(9009);
  });
  it('Should find largest from 2 digit integers', () => {
    expect(tests.test1(3)).toBe(906609);
  });
});