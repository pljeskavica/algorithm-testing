import {test1, test2} from '../3-largestPrimeFactor.js';

describe("Tests should find largest prime factor using test 1",  () => {
  it("Should find the ones for 10", () => {
    expect(test1(10)).toBe(5);
  })
  it("Should find the ones for 10", () => {
    expect(test1(13195)).toBe(29);
  })
})

describe("Tests should find largest prime factor using test 2",  () => {
  it("Should find the ones for 10", () => {
    expect(test2(10)).toBe(5);
  })
  it("Should find the ones for 10", () => {
    expect(test2(13195)).toBe(29);
  })
})
