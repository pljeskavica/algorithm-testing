import {test1, test2} from '../2-evenFibNumbers.js';

describe("Tests should find the sum of the even valued terms in the fibonacci sequence Using test 1",  () => {
  it("Should find the ones under 10", () => {
    expect(test1(10)).toBe(10);
  })
  it("Should find the ones under 4000000", () => {
    expect(test1(4000000)).toBe(4613732);
  })
})

describe("Tests should find the sum of the even valued terms in the fibonacci sequence Using test 2",  () => {
  it("Should find the ones under 10", () => {
    expect(test2(2, 1, 2, 10)).toBe(10);
  })
  it("Should find the ones under 4000000", () => {
    expect(test2()).toBe(4613732);
  })
})