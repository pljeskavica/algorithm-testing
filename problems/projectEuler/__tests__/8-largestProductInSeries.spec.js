import data from '../../data/series'
import {test1, test2} from '../8-largestProductInSeries';


describe('Should find the largest product using adjacent digits using approach 1', () => {
  it('Should find the largest product from four adjacent digits', () => {
    expect(test1(data, 4)).toBe(5832)
  });
  it('Should find the largest product from four adjacent digits', () => {
    expect(test1(data, 13)).toBe(23514624000)
  });
});


describe('Should find the largest product using adjacent digits using approach 2', () => {
  it('Should find the largest product from four adjacent digits', () => {
    expect(test2(data, 4)).toBe(5832)
  });
  it('Should find the largest product from four adjacent digits', () => {
    expect(test2(data, 13)).toBe(23514624000)
  });
});