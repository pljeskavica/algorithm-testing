import {test1} from '../6-sumSquareDifference';

describe('Should find the difference between sum of the squares and the square of the sum of first n natural numbers using approach 1', () => {

  it('It to find the sum square difference of the first 5 natural numbers', () => {
    expect(test1(5)).toBe(170)
  });
  it('It to find the sum square difference of the first 10 natural numbers', () => {
    expect(test1(10)).toBe(2640)
  });
  it('It to find the sum square difference of the first 100 natural numbers', () => {
    expect(test1(100)).toBe(25164150)
  });
});

describe('Should find the difference between sum of the squares and the square of the sum of first n natural numbers using approach 2', () => {

  it('It to find the sum square difference of the first 5 natural numbers', () => {
    expect(test1(5)).toBe(170)
  });
  it('It to find the sum square difference of the first 10 natural numbers', () => {
    expect(test1(10)).toBe(2640)
  });
  it('It to find the sum square difference of the first 100 natural numbers', () => {
    expect(test1(100)).toBe(25164150)
  });
});