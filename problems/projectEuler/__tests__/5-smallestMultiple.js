import {test1, test2, test3} from '../5-smallestMultiple';

describe('Find the smallest multiple of all numbers 1-n using approach 1', () => {
  it('Should find 1-2', () => {
    expect(test1(2)).toBe(2)
  });
  it('Should find 1-10', () => {
    expect(test1(10)).toBe(2520)
  });
  it('Should find 1-20', () => {
    expect(test1(20)).toBe(232792560)
  });
});

describe('Find the smallest multiple of all numbers 1-n using approach 2', () => {
  it('Should find 1-2', () => {
    expect(test2(2)).toBe(2)
  });
  it('Should find 1-10', () => {
    expect(test2(10)).toBe(2520)
  });
  it('Should find 1-20', () => {
    expect(test2(20)).toBe(232792560)
  });
});

// Function does not work, waiting for TCO Optimization support in node

// describe('Find the smallest multiple of all numbers 1-n using approach 3', () => {
//   it('Should find 1-2', () => {
//     expect(test3(2)).toBe(2)
//   });
//   it('Should find 1-10', () => {
//     expect(test3(10)).toBe(2520)
//   });
//   it('Should find 1-20', () => {
//     expect(test3(20)).toBe(232792560)
//   });
// });