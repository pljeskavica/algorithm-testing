// Find the largest palindrome from two integers given the number of digits of the numbers

// O(n^2) Time
export const test1 = (digits) => {
  // Create highest Palindrome holder 
  let highestPalindrome = 0;
  // Iterate through numbers with that many digits
  // Starting at 1 + the number of 0's in digits - 1  Going until i.length < digits
  for(let i = parseInt(`1${'0'.repeat((digits - 1))}`); String(i).length < digits+1; i++){
    // Do a nested Iteration 
    // Iterate through numbers with that many digits
    // Starting at 1 + the number of 0's digits - 1 Going until i.length < digits
    for(let j = parseInt(`1 + ${'0'.repeat((digits - 1))}`); String(j).length < digits+1; j++){
      // Multiply i and j
      const product = i * j;
      // Test if product is a palindrome
      // Convert to string
      const origString = String(product);
      // Reverse string
      const revString = origString.split("").reverse().join("");
      // Test if its equal to the original
      if(origString === revString){
        // Test if its larger than palindrome holder
        if(product > highestPalindrome){
          // If it is set to new holder
          highestPalindrome = product;
        }
      }
    }
  }
  // Return holder
  return highestPalindrome;
}

// // O(n^2) Time
// export const test2 = (digits ) => {
//   // Nest this function because we need to do calculations on digits to get default arguments
//   const findPalindrome = (i=parseInt(`1${'0'.repeat((digits - 1))}`), j=parseInt(`1${'0'.repeat((digits - 1))}`), highestPalindrome=0, maxLength=digits) => {
//     // Multiply i and j
//     const product = i * j;
//     // Test if product is a palindrome
//     // Convert to string
//     const origString = String(product);
//     // Reverse string
//     const revString = origString.split("").reverse().join("");
//     // Test if its equal to the original
//     if(origString === revString){
//       // Test if its larger than palindrome holder
//       if(product > highestPalindrome){
//         // If it is set to new holder
//         return findPalindrome(i+1, j+)
//         highestPalindrome = product;
//       }
//     }
//   }
//   // Create highest Palindrome holder 
//   let highestPalindrome = 0;
//   // Iterate through numbers with that many digits
//   // Starting at 1 + the number of 0's in digits - 1  Going until i.length < digits
//   for(let i = parseInt(`1${'0'.repeat((digits - 1))}`); String(i).length < digits+1; i++){
//     // Do a nested Iteration 
//     // Iterate through numbers with that many digits
//     // Starting at 1 + the number of 0's digits - 1 Going until i.length < digits
//     for(let j = parseInt(`1 + ${'0'.repeat((digits - 1))}`); String(j).length < digits+1; j++){
//       // Multiply i and j
//       const product = i * j;
//       // Test if product is a palindrome
//       // Convert to string
//       const origString = String(product);
//       // Reverse string
//       const revString = origString.split("").reverse().join("");
//       // Test if its equal to the original
//       if(origString === revString){
//         // Test if its larger than palindrome holder
//         if(product > highestPalindrome){
//           // If it is set to new holder
//           highestPalindrome = product;
//         }
//       }
//     }
//   }
//   // Return holder
//   return highestPalindrome;
// }