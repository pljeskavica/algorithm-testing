export const test1 = n => {
  let sum = 0;
  for (let i = 0; i < n; i++){
    if(i%3 === 0){
      sum += i;
      continue;
    }
    if(i%5 === 0){
      sum += i;
      continue;
    }
    continue;
  }
  return sum;
}

export const test2 = n => {
  let sum = 0;
  for (let i = 0; i < n; i++){
    if(i%5 === 0){
      sum += i;
      continue;
    }
    if(i%3 === 0){
      sum += i;
      continue;
    }
    continue;
  }
  return sum;
}

export const test3 = n => {
  let sum = 0;
  for (let i = 0; i < n; i++){
    if(i%3 === 0 || i%5 === 0){
      sum += i;
      continue;
    }
    continue;
  }
  return sum;
}