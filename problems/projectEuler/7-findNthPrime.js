// Find the nth prime number

// O(n^2) Time
// O(1) Memory
export const test1 = (n) => {
  // Create prime counter
  let primeCounter = 0;
  // Iterate through numbers from 2
  for(let i = 2; primeCounter <= n; i++){
    // Nest another itearation checking to see if any number below it is divisible by anthing other than itself and 1
    // Create a prime flag, and assume it is prime until we find a number it can be divided against
    let primeFlag = true;
    for(let j = 2; j<i; j++){
      if(i%j === 0){
      // If Its not then its a prime, increate prime counter
        primeFlag = false;
        break;
      }
      continue;
    }
    if(primeFlag){
      primeCounter++;
      // If prime counter is equal to n return this prime number which is i from the original iteration loop
      if(primeCounter===n){
        return i;
      }
    }
  }
  return 0;
}

// O(n^~1.3) Time Testing only primes lower than itself reduces it quite a bit
export const test2 = (n) => {
  // Create prime counter
  let primeCounter = 1,
      primeHolder = [2];
  // Iterate through numbers from 2 which will be our starting number
  for(let i = 3; primeCounter <= n; i++){
    // Nest another itearation checking to see if any prime number below it can be cleanly divided
    // Create a prime flag, and assume it is prime until we find a number it can be divided against
    let primeFlag = true;
    for(let j = 0; j<primeHolder.length; j++){
      if(i%primeHolder[j] === 0){
      // If Its not then its a prime, increate prime counter
        primeFlag = false;
        break;
      }
      continue;
      // If prime counter is equal to n return this prime number which is i from the original iteration loop
    }
    if(primeFlag){
      primeCounter++;
      if(primeCounter===n){
        return i;
      }
      primeHolder.push(i)
    }
  }
  return 0;
}

// O(n^~1.1) Significantly reduces the number of tests by using square root
export const test3 = (n) => {
  // Create prime counter
  let primeCounter = 1,
      primeHolder = [2];
  // Iterate through numbers from 2 which will be our starting number
  for(let i = 3; primeCounter <= n; i+=2){
    // Nest another itearation checking to see if any prime number below it can be cleanly divided
    // Create a prime flag, and assume it is prime until we find a number it can be divided against
    let primeFlag = true;
    for(let j = 0; j<primeHolder.length; j++){
      if(i%primeHolder[j] === 0){
      // If Its not then its a prime, increate prime counter
        primeFlag = false;
        break;
      }
      continue;
      // If prime counter is equal to n return this prime number which is i from the original iteration loop
    }
    if(primeFlag){
      primeCounter++;
      if(primeCounter===n){
        return i;
      }
      primeHolder.push(i)
    }
  }
  return 0;
}

export const test4 = (n) => {
  // Create prime counter
  let primeCounter = 1,
      primeHolder = [2];
  // Iterate through numbers from 2 which will be our starting number
  loop1: for(let i = 3; primeCounter <= n; i+=2){
            // Nest another itearation checking to see if any prime number below it can be cleanly divided
            for(let r = Math.sqrt(i), j = 0; primeHolder[j]<=r; j++){
              if(i%primeHolder[j] === 0){
              // If Its not then its a prime, increate prime counter
                continue loop1;
              }

              // If prime counter is equal to n return this prime number which is i from the original iteration loop
            }
            primeCounter++;
            if(primeCounter===n){
              return i;
            }
            primeHolder.push(i)
          }
  return 0;
}

// From Daniel-Hug
// https://github.com/Daniel-Hug/JS-nth-prime/blob/master/nth-prime-calculator.js
export const test5 = (n) =>{
  l: for (var primes = [2], i = 3; primes.length < n; i += 2) {
        for (let root = Math.sqrt(i), j = 0; primes[j] <= root; j++) {
          if (i%primes[j] === 0) continue l;
        }
      primes.push(i);
  }
  return primes[n - 1];
}