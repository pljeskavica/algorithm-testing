

export const test1 = (n) => {
  // Iterate to n where n is the upper limit of numbers we need to be divisible by
  // Use highestMultiple to track the current number we are dividing against
  // Start highestMultiple at n because it cant be smaller than n
  // When We cant divide against it evenly set i back to 0 and increment j
  let highestMultiple = n;
  for(let i = 1; i <= n; i++){
    if(highestMultiple % i === 0){
      continue;
    }else{
      highestMultiple +=1;
      i = 1;
    }
  }
  return highestMultiple;
}

export const test2 = (n) => {
  // Iterate to n where n is the upper limit of numbers we need to be divisible by
  // Use highestMultiple to track the current number we are dividing against
  // Start highestMultiple at n because it cant be smaller than n
  // When We cant divide against it evenly set i back to 0 and increment j
  let highestMultiple = n;
  for(let i = n; i >= 1; i--){
    if(highestMultiple % i === 0){
      continue;
    }else{
      highestMultiple +=1;
      i = n;
    }
  }
  return highestMultiple;
}

// TCO Optimization is not currently supported in Node even with a 9.0.0 nightly build
// Check http://node.green/#ES2015-optimisation-proper-tail-calls--tail-call-optimisation--direct-recursion
// For current status
// Until it is supported this function will not work as it is O(n) memory without TCO Optimization

// export const test3 = (n) => {
//   const findMultiple = (upperLimit,highestMultiple, i=1) => {
//     console.log('Start')
//       console.log(upperLimit, highestMultiple, i)
//     if(i === upperLimit){
//       console.log(`Found the highestMultiple at ${highestMultiple}`)
//       return highestMultiple;
//     }
//     if(highestMultiple%i === 0){
//       console.log('Its A Multiple!')
//       console.log(`${highestMultiple} is a multiple of ${i}`)
//       console.log(upperLimit, highestMultiple, i+1)
//       return findMultiple(upperLimit, highestMultiple, i+1,);
//     }
//     console.log('Its not a Multiple!')
//     console.log(`${highestMultiple} is not a multiple of ${i}`)
//     console.log(upperLimit, highestMultiple+1, 1)
//     return findMultiple(upperLimit, highestMultiple+1, 1);
//   }
//   console.log(`Starting Test3 with n: ${n}`)
//   return findMultiple(n, n);
// }