export const test1 = (ceiling) => {
  // Store self in holder
  // Add self plus previous number
  // Set previous number to the holder value
  // if self is even add to sum
  let holder = 0,
      previousN = 1,
      currentN = 2,
      sum = 2;
  while (currentN < ceiling){
    holder = currentN;
    currentN += previousN
    previousN = holder;
    if(currentN%2 === 0){
      sum += currentN
    }
  }
  return sum;
}

export const test2 = (currentN=2, previousN=1, sum=2, ceiling=4000000) => {
  // Store self in holder
  // Add self plus previous number
  // Set previous number to the holder value
  // if self is even add to sum
  let newN = currentN + previousN;
  if(newN > ceiling){
    return sum;
  }
  if(newN%2 === 0){
    sum += newN
  }
  return test2(newN, currentN, sum, ceiling);
}


// runTest(1, test1, 4000000);

