// Find highest product of n contiguous digits in string (data)

export const test1 = (data, n) => {
  // Create a holder for the highest product
  let highestProduct = 0; 
  // Iterate through data string starting at 0, and incrementing by n
  for(let i = 0; i < data.length; i++){
    // Create string from digits i-i+4(End on slice is not inclusive)
    // Split the string, multiply them together
    let splitString = data.slice(i, i+n).split(''),
          product = parseInt(splitString[0]);
    for(let j=1; j< splitString.length; j++){
      product *= parseInt(splitString[j]);
    }
    // If its higher than the holder new holder is = to product
    highestProduct = highestProduct > product ? highestProduct : product;
  }
  // Return holder
  return highestProduct;
}

export const test2 = (data, n) => {
  // Create a holder for the highest product
  let highestProduct = 0; 
  // Iterate through data string starting at 0, and incrementing by n
  for(let i = 0; i < data.length - n + 1; i++){
    let product = parseInt(data[i]);
    for(let j=i+1; j< i+n; j++){
      product *= parseInt(data[j]);
    }
    // If its higher than the holder new holder is = to product
    highestProduct = highestProduct > product ? highestProduct : product;
  }
  // Return holder
  return highestProduct;
}