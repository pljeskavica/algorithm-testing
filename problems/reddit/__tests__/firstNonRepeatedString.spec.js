import {test1, test2, test3} from '../firstNonRepeatedString';
import * as Data from '../data/strings';

describe('Find the first non repeated character in a string using approach 1', () => {

  it('Should find one in a short string', () => {
    expect(test1(Data.shortString)).toBe('w')
  });
  it('Should find one in a medium string', () => {
    expect(test1(Data.mediumString)).toBe('q')
  });
  it('Should find one in a long string', () => {
    expect(test1(Data.longString)).toBe('}')
  });
});

describe('Find the first non repeated character in a string using approach 2', () => {

  it('Should find one in a short string', () => {
    expect(test2(Data.shortString)).toBe('w')
  });
  it('Should find one in a medium string', () => {
    expect(test2(Data.mediumString)).toBe('q')
  });
  it('Should find one in a long string', () => {
    expect(test2(Data.longString)).toBe('}')
  });
});

// describe('Find the first non repeated character in a string using approach 3', () => {

//   it('Should find one in a short string', () => {
//     expect(test3(Data.shortString)).toBe('w')
//   });
//   it('Should find one in a medium string', () => {
//     expect(test3(Data.mediumString)).toBe('q')
//   });
//   it('Should find one in a long string', () => {
//     expect(test3(Data.longString)).toBe('}')
//   });
// });