import { test1, test2, test3} from '../multiplyWithoutStar';

describe('Multiply two integers without using * using Approach 1', () => {

  it('Should work with positive integers', () => {
    expect( test1(2, 50)).toBe(100)
  });
  it('Should work with two negative integers', () => {
    expect( test1(-2, -50)).toBe(100)
  });
  it('Should work with one 0 integer', () => {
    expect( test1(0, 50)).toBe(0)
  });
  it('Should work with large integers', () => {
    expect( test1(21000, 5000054654)).toBe(105001147734000)
  });
  it('Should work with one negative integer', () => {
    expect( test1(2, -50)).toBe(-100)
  });
});

describe('Multiply two integers without using * using Approach 2', () => {

  it('Should work with positive integers', () => {
    expect( test2(2, 50)).toBe(100)
  });
  it('Should work with two negative integers', () => {
    expect( test2(-2, -50)).toBe(100)
  });
  it('Should work with one 0 integer', () => {
    expect( test2(0, 50)).toBe(0)
  });
  // it('Should work with large integers', () => {
  //   expect( test2(21000, 5000054654)).toBe(105001147734000)
  // });
  it('Should work with one negative integer', () => {
    expect( test2(2, -50)).toBe(-100)
  });
});

describe('Multiply two integers without using * using Approach 3', () => {

  it('Should work with positive integers', () => {
    expect( test3(2, 50)).toBe(100)
  });
  it('Should work with two negative integers', () => {
    expect( test3(-2, -50)).toBe(100)
  });
  it('Should work with one 0 integer', () => {
    expect( test3(0, 50)).toBe(0)
  });
  // it('Should work with large integers', () => {
  //   expect( test3(21000, 5000054654)).toBe(105001147734000)
  // });
  it('Should work with one negative integer', () => {
    expect( test3(2, -50)).toBe(-100)
  });
});