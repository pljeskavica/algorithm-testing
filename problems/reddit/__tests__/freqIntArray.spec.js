import * as Arrays from '../data/arrays';
import {test1, test2, test3, test4} from '../freqIntArray';

describe('Should find the most frequent integer in the array using Approach 1', () => {
  it('Should work on short arrays', () => {
    expect(test1(Arrays.shortArray)).toBe(4);
  });
  it('Should work on medium arrays', () => {
    expect(test1(Arrays.mediumArray)).toBe(1);
  });
  it('Should work on long arrays', () => {
    expect(test1(Arrays.longArray)).toBe(123);
  });
});

describe('Should find the most frequent integer in the array using Approach 2', () => {
  it('Should work on short arrays', () => {
    expect(test2(Arrays.shortArray)).toBe(4);
  });
  it('Should work on medium arrays', () => {
    expect(test2(Arrays.mediumArray)).toBe(1);
  });
  it('Should work on long arrays', () => {
    expect(test2(Arrays.longArray)).toBe(123);
  });
});

describe('Should find the most frequent integer in the array using Approach 3', () => {
  it('Should work on short arrays', () => {
    expect(test3(Arrays.shortArray)).toBe(4);
  });
  it('Should work on medium arrays', () => {
    expect(test3(Arrays.mediumArray)).toBe(1);
  });
  it('Should work on long arrays', () => {
    expect(test3(Arrays.longArray)).toBe(123);
  });
})

describe('Should find the most frequent integer in the array using Approach 4', () => {
  it('Should work on short arrays', () => {
    expect(test4(Arrays.shortArray)).toBe(4);
  });
  it('Should work on medium arrays', () => {
    expect(test4(Arrays.mediumArray)).toBe(1);
  });
  it('Should work on long arrays', () => {
    expect(test4(Arrays.longArray)).toBe(123);
  });
})