// Multiply two integers without using *

export const test1 = (x, y) => {
  if(x === 0  || y === 0){
    return 0;
  }
  return x /( y / (Math.pow(y, 2)))
}

export const test2 = (x, y) => {
  let sum = 0;
  let counter = Math.abs(y);
  for(let i = 0; i < counter; i++){
    sum += x;
  }
  if(y < 0){
    return -sum;
  }
  return sum;
}

export const test3 = (x, y) => {
  if(Math.abs(x) > Math.abs(y)){
    let sum = 0;
    let counter = Math.abs(y);
    for(let i = 0; i < counter; i++){
      sum += x;
    }
    if(y < 0){
      return -sum;
    }
    return sum;
  }else{
    let sum = 0;
    let counter = Math.abs(x);
    for(let i = 0; i < counter; i++){
      sum += y;
    }
    if(x < 0){
      return -sum;
    }
    return sum;
  }
}

// export const test2 = (x, y) => {
//   let sum = 0;
//   let counter = Math.abs(y);
//     for(let i = 0; i < counter; i++){
//       sum += x;
//     }
//     if(y < 0){
//       return -sum;
//     }
//     return sum;
// }

// export const test3 = (x, y) => {
//   if(Math.abs(x) > Math.abs(y)){
//     let sum = 0;
//     let counter = Math.abs(y);
//     for(let i = 0; i < counter; i++){
//       sum += x;
//     }
//     if(y < 0){
//       return -sum;
//     }
//     return sum;
//   }else{
//     let sum = 0;
//     let counter = Math.abs(x);
//     for(let i = 0; i < counter; i++){
//       sum += y;
//     }
//     if(x < 0){
//       return -sum;
//     }
//     return sum;
//   }
// }