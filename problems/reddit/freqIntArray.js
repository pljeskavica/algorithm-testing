// Find the most frequent occurring integer in an array of integers

export const test1 = (arr) => {
  const occurrences = {};
  for(let i = 0; i < arr.length; i++){
    if(occurrences[arr[i]] === undefined){
      occurrences[arr[i]] = 1;
      continue;
    }
    occurrences[arr[i]] += 1;
    continue;
  }
  let highestInt,
      occurrence = 0;
  for(let k in occurrences){

    if(occurrences[k] > occurrence){
      highestInt = k;
      occurrence = occurrences[k]
    }
  }
  return parseInt(highestInt);
}

export const test2 = (arr) => {
  const occurrences = {};
  let highestInt = arr[0],
      occurrence = 1;
  for(let i = 0; i < arr.length; i++){
    if(occurrences[arr[i]] === undefined){
      occurrences[arr[i]] = 1;
      continue;
    }
    occurrences[arr[i]] += 1;
    if(occurrences[arr[i]] > occurrence){
      highestInt = arr[i];
      occurrence = occurrences[arr[i]];
    }
    continue;
  }
  return highestInt;
}

export const test3 = (arr) => {
  const countOccurrences = (arr, occurrences = {}) => {
    if(arr.length === 0){
      return occurrences;
    }
    if(occurrences[arr[0]] === undefined){
      occurrences[arr[0]] = 1;
      const newArr = arr.slice(1);
      return countOccurrences(newArr, occurrences);
    }
    occurrences[arr[0]] += 1;
    const newArr = arr.slice(1);
    return countOccurrences(newArr, occurrences);
  }

  const calculateHighestOccurrence = (occurrences, keys=Object.keys(occurrences), highestInt = undefined, occurrence = 0) => {
    if(keys.length === 0){
      let int = parseInt(highestInt)
      return int;
    }
    if(occurrences[keys[0]] > occurrence){
      const newHighestInt = keys[0],
          newOccurence = occurrences[keys[0]];
      keys.shift();
      return calculateHighestOccurrence(occurrences, keys, newHighestInt, newOccurence )
    }
    keys.shift();
    return calculateHighestOccurrence(occurrences, keys, highestInt, occurrence )
  }

  return calculateHighestOccurrence(countOccurrences(arr))
}


export const test4 = (arr, occurrences = {}, highestInt=null, occurrence=0) => {
  if(arr.length === 0){
    return highestInt;
  }
  if(occurrences[arr[0]] === undefined){
    occurrences[arr[0]] = 1;
    if (1 > occurrence){
      highestInt = arr[0];
      occurrence = 1;
    }
    const newArr = arr.slice(1);
    return test4(newArr, occurrences, highestInt, occurrence);
  }
  occurrences[arr[0]] += 1;
  if (occurrences[arr[0]] > occurrence){
    highestInt = arr[0];
    occurrence = occurrences[arr[0]];
  }
  const newArr = arr.slice(1);
  return test4(newArr, occurrences, highestInt, occurrence);
}