// Find the first non repeated character in a string

// O(n) - Must Go through Every character
export const test1 = (s) => {
  // Create Holder Array
  const holderArray = [];
  const repeatedChars = [];
  // Iterate through the string
  for(let i = 0; i < s.length; i++){
    // Check if char is in holder array
    if(holderArray.indexOf(s[i]) != -1){
      // If it is then remove it from holder array
      holderArray.splice(holderArray.indexOf(s[i]), 1)
      repeatedChars.push(s[i])
    }else if(repeatedChars.indexOf(s[i]) === -1){
      // If not then push it onto array
      holderArray.push(s[i])
    }else{
      continue;
    }
  }
  // Return holderArray[0]
  return holderArray[0];
}

// O(n) - Much faster because once we hit the right answer it will finish
export const test2 = (s) => {
  const nonUniqueValues = [];
  for(let i = 0; i < s.length; i++){
    if(s.indexOf(s[i], i+1) === -1 && nonUniqueValues.indexOf(s[i]) === -1){
      return s[i];
    }
    nonUniqueValues.push(s[i]);
  }
}

// This will not work due to TCO not supported in node currently
// export const test3 = (s) => {
//   if(s.indexOf(s[0], 1) === -1){
//     return s[0];
//   }
//   const badLetters = `.\+*?[^]$(){}=!<>|:-`;
//   const letter = (badLetters.indexOf(s[0], 1) === -1) ? s[0] : `\${s[0]}`;
//   const re = new RegExp(letter, "g")
//   const newS = s.replace(re, '')
//   return test3(newS)
// }