import runTests from './util/runTest';
import * as tests from './problems/reddit/firstNonRepeatedString.js';
// import * as tests from './problems/projectEuler/8-largestProductInSeries';


import * as Data from './problems/reddit/data/strings';

const functionList = [tests.test1, tests.test2, tests.test3];
const args = [Data.longStringFlipped];

runTests(functionList, args, 10000)
